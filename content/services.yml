---
  title: Professional Services
  description: Need help installing, upgrading, or migrating to GitLab? We can help with migration services or simply upgrading your current instance.
  hero:
      title: GitLab Professional Services
      subtitle: Reduce time to market by accelerating the adoption of modern software delivery methods.
      modal_src: https://www.youtube.com/embed/FmimIGNR2S4?autoplay=1
      aos_animation: fade-down
      aos_duration: 500
      img_animation: zoom-out-left
      img_animation_duration: 1600
      primary_btn:
        url: "#contactform"
        text: Request Service
        data_ga_name: request service
        data_ga_location: header
      secondary_btn:
        modal: true
        data_ga_name: watch a demo
        data_ga_location: header
        text: Watch a demo
        variant: tertiary
        icon:
          name: play
          variant: product
      image:
        image_url: /nuxt-images/services/hero.jpeg
        alt: "Image: GitLab Professional Services"
        bordered: true
  side_menu:
    anchors:
      text: On this page
      data:
        - text: Implementation Services
          href: "#implementation-services"
          data_ga_name: implementation services
          data_ga_location: side-navigation
        - text: Migration Services
          href: "#migration-services"
          data_ga_name: migration services
          data_ga_location: side-navigation
        - text: Education Services
          href: "#education-services"
          data_ga_name: education services
          data_ga_location: side-navigation
        #- text: Integration Services
        #  href: "#integration-services"
        #  data_ga_name: integration services
        #  data_ga_location: side-navigation
        - text: Dedicated Engineering
          href: "#dedicated-engineering"
          data_ga_name: dedicated engineering
          data_ga_location: side-navigation
        - text: Advisory Services
          href: "#advisory-services"
          data_ga_name: advisory services
          data_ga_location: side-navigation
    hyperlinks:
      data:
        - text: Request Services
          href: "#contactform"
          variant: primary
          data_ga_name: request services
          data_ga_location: side-navigation
        - text: Watch a demo
          modal: true
          icon:
            name: play
            variant: product
          variant: tertiary
          data_ga_name: learn-about-ultimate
          data_ga_location: side-navigation
  blog_copy:
    block:
      - header: Implementation Services
        id: implementation-services
        sections:
          - type: copy
            text: |
              Whether your team chose GitLab to accelerate your DevSecOps transformation, improve collaboration between all the roles in your organization, or facilitate an innersourcing initiative - Now the challenge is to get to that desired end state as fast as possible. Though thousands have deployed our open source tooling throughout the world, we also have a team of professionals here to provide you resources to ensure your GitLab instance will scale with your team.

              There are three different ways to get started with Professional Services from GitLab.
          - type: card
            title: Rapid Results Consulting
            text: |
              The GitLab Rapid Results Consulting package helps you quickly implement your GitLab solution and enable your organization to take advantage of your GitLab purchase quickly.
            link:
              text: Learn more
              href: /services/rapid-results/
              data_ga_name: rapid results consulting
              data_ga_location: body
          - type: card
            title: Dedicated Implementation Planning
            text: |
              For customers who need to plan for on-going scaling of their GitLab instance, high availability and disaster recovery for large-scale deployments of thousands of users we recommend more implementation focus from our team of Professional Services Engineers.
            link:
              text: Learn more
              href: /services/implementation/enterprise/
              data_ga_name: dedicated implementation planning
              data_ga_location: body
          - type: card
            title: Health Check
            text: |
              Let's evaluate your deployment architecture to provide recommendations on how to optimize for performance, stability, and availability.
            link:
              text: Learn more
              href: /services/implementation/health-check/
              data_ga_name: health check
              data_ga_location: body
      - header: Migration Services
        id: migration-services
        sections:
          - type: copy
            text: |
              Your data is valuable and making it readily available for immediate continuity is indispensable. Our migration services will accelerate your transition to GitLab so you can focus on your business.

              There are two different ways to get started with Migration Services from GitLab.
          - type: card
            column_size: 6
            title: Migration+ Package
            text: |
              The GitLab Migration+ package enables most companies to migrate their data from previous source code management systems and train their users and adminstrators with GitLab fundamentals.
            link:
              text: Learn more
              href: /services/migration/migration-plus/
              data_ga_name: migration package
              data_ga_location: body
          - type: card
            column_size: 6
            title: Custom-Scoped Migration
            text: |
              For larger customers who have more users and repositories and may have more complex data migration requirements including multiple source systems.
            link:
              text: Learn more
              href: /services/migration/enterprise/
              data_ga_name: custom scoped migration
              data_ga_location: body
      - header: Education Services
        id: education-services
        sections:
          - type: copy
            text: |
              Your teams are composed of highly proficient technical experts. With any comprehensive solution, there is a learning curve to consider. Our product specialists—technology professionals themselves—are available to train your teams to become efficient in GitLab and DevSecOps quickly.

              We are working to make all of our educational offerings more widely available by updating our current learning infrastructure to support a broader audience. We will be posting updates to our GitLab Blog as we ready our new platform. Be sure to subscribe to see the latest updates.
          - type: copy
            title: Who is this for?
            text: |
              Anyone looking to cut "under the curve" of learning not only GitLab but Concurrent DevSecOps best practices.
          - type: card
            title: What is included?
            text: |
              GitLab offers a variety of courses delivered at your site or remotely by our experienced GitLab trainers. In addition, we offer specialized training that can be customized even further to meet your team's unique needs.

              Here are the standard courses we offer:

              - [GitLab with Git Essentials Training](/services/education/gitlab-basics/){data-ga-name="gitlab with git basics training" data-ga-location="body"}

              - [GitLab CI/CD Training](/services/education/gitlab-ci/){data-ga-name="gitlab ci/cd training" data-ga-location="body"}

              - [GitLab for Project Managers Training](/services/education/pm/){data-ga-name="gitlab for project managers training" data-ga-location="body"}

              - [GitLab Security Essentials Training](/services/education/security-essentials/){data-ga-name="gitlab security essentials training" data-ga-location="body"}

              - [GitLab DevOps Fundamentals Training](/services/education/devops-fundamentals/){data-ga-name="gitlab devops fundamentals training" data-ga-location="body"}

              - [GitLab System Administration Training](/services/education/admin/){data-ga-name="gitlab system administration training" data-ga-location="body"}

              Here are the standard technical certifications we offer:

              - [GitLab Certified Git Associate](/services/education/){data-ga-name="gitlab certified git associate" data-ga-location="body"}

              - [GitLab Certified CI/CD Associate](/services/education/gitlab-cicd-associate/){data-ga-name="gitlab certified ci/cd associate" data-ga-location="body"}

              - [GitLab Certified Project Management Associate](/services/education/gitlab-project-management-associate/){data-ga-name="gitlab certified project management associate" data-ga-location="body"}

              - [GitLab Certified Security Specialist](/services/education/gitlab-security-specialist/){data-ga-name="gitlab certified security specialist" data-ga-location="body"}

              - [GitLab Certified DevOps Professional](/services/education/gitlab-certified-devops-pro/){data-ga-name="gitlab certified ci/cd specialist" data-ga-location="body"}              


              For organizations that need to scale training to a larger audience, we also offer a Train the Trainer option.

              For a complete list of Professional Services offerings visit the [Full Catalog page](https://about.gitlab.com/services/catalog/).

      #- header: Integration Services
      #  id: integration-services
      #  sections:
      #    - type: copy
      #      text: |
      #        Your team has to have a centralized DevSecOps platform to be effective. Our integration services will facilitate your integration of external tools to GitLab by providing professional, best-of-breed integrations with the other tools you use every day.
      #    - type: copy
      #      title: Who is this for?
      #      text: |
      #        Customers who need to integrate GitLab to existing enterprise systems
      #    - type: card
      #      title: What is included?
      #      list_style: bullet
      #      text: |
      #        Examples of the types of integrations our team can facilitate:

      #        - LDAP / SAML / SSO

      #       - JIRA

      #        - Jenkins

      - header: Dedicated Engineering
        id: dedicated-engineering
        sections:
          - type: copy
            text: |
              Your team has to have a centralized DevSecOps platform to be effective. Our integration services will facilitate your integration of external tools to GitLab by providing professional, best-of-breed integrations with the other tools you use every day.
          - type: copy
            title: Who is this for?
            text: |
              - Customers who need technical and/or strategic guidance on how to get the most from their GitLab investment.

              - Customers who do not have capacity or relevant skill set to implement strategic guidance themselves.

              - Customers who are looking to transfer knowledge from GitLab Professional Services Engineers to their team by having one or multiple GitLab Professional Services Engineer(s) join your team.

              - Customers who might not know exactly what topics they need help with, but know that services will help maximize the value they get from GitLab.
          - type: card
            title: What is included?
            list_style: number
            text: |
              We offer dedicated engineering engagements for three set durations, 3, 6 and 12 months. During these engagements a PS Engineer will work with you full time, focusing on building solutions to business problems using GitLab. As the engagements get longer, the prices are discounted. Please see the below engagement SOWs for pricing and specific activities for this engagement.

              - [Dedicated Engineer - 3 Months](https://docs.google.com/document/d/1YGh2j9WZNkC9TQTqds3Q-8MjmEgCnfpo/edit?usp=share_link&ouid=110714040742704238151&rtpof=true&sd=true){data-ga-name="dedicated engineer - 3 months" data-ga-location="body"}
              - [Dedicated Engineer - 6 Months](https://docs.google.com/document/d/1ejRhREpQQCS2s9NFWET1-zc1C4nhdUlw/edit?usp=share_link&ouid=110714040742704238151&rtpof=true&sd=true){data-ga-name="dedicated engineer - 6 months" data-ga-location="body"}
              - [Dedicated Engineer - 12 Months](https://docs.google.com/document/d/1I9i7lKjyfHy6tH7-kWAE49q1DM5mVWIL/edit?usp=share_link&ouid=110714040742704238151&rtpof=true&sd=true){data-ga-name="dedicated engineer - 12 months" data-ga-location="body"}
              - [Dedicated Engineer with Security Clearance - 3 Months](https://docs.google.com/document/d/1Lehhw7CrxfbIz9mpS__Cy7Ib2JbbLmtu/edit?usp=share_link&ouid=110714040742704238151&rtpof=true&sd=true){data-ga-name="dedicated engineer with security clearance - 3 months" data-ga-location="body"}
              - [Dedicated Engineer with Security Clearance - 6 Months](https://docs.google.com/document/d/1xrBbocmzLP19e6t5SiElIOx6RfcXcHHj/edit?usp=share_link&ouid=110714040742704238151&rtpof=true&sd=true){data-ga-name="dedicated engineer with security clearance - 6 months" data-ga-location="body"}
              - [Dedicated Engineer with Security Clearance - 12 Months](https://docs.google.com/document/d/1_M5uOnYKy-lUaVj1_YF_xq05abM5Z9mJ/edit?usp=share_link&ouid=110714040742704238151&rtpof=true&sd=true){data-ga-name="dedicated engineer with security clearance - 12 months" data-ga-location="body"}
      - header: Advisory Services
        id: advisory-services
        sections:
          - type: copy
            text: |
              The GitLab platform provides an opportunity to transform the way your business operates. But making that transition from your legacy way of working to a more efficient and effective value stream can be challenging. Engage with GitLab experts to solve specific challenges or keep a GitLab Expert on retainer to help guide you on your journey.

              GitLab provides the following offerings that differ in commercial structure and methodology.
          - type: card
            title: Advisory Workshop
            text: |
              Advisory workshops focus on a specific workflow, feature, or functional section of GitLab. We learn about your goals and provide an example of how to solve the problem with GitLab.
            link:
              text: Learn more
              href: /services/advisory/advisory-workshop
              data_ga_name: advisory workshop
              data_ga_location: body
          - type: card
            title: Expert Services
            text: |
              For customers who might not know exactly what they need help with at the time of their license purchase/expansion, we can provide guidance on a wide range of topics including best practices, workflows, GitLab implementation, and data migration.
            link:
              text: Learn more
              href: /services/advisory/expert-services/
              data_ga_name: advisory workshop
              data_ga_location: body
  form_block:
    id: 'contactform'
    header: Interested in GitLab Professional Services? Get in touch.
    form:
      form_id: 1476
      datalayer: services
