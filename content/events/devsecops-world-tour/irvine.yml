---
  title: "Irvine World Tour"
  og_title: Irvine World Tour
  description: Join us at the GitLab DevSecOps World Tour in Irvine where we’ll be showcasing a comprehensive overview of the evolving DevSecOps landscape.
  og_description: Join us at the GitLab DevSecOps World Tour in Irvine where we’ll be showcasing a comprehensive overview of the evolving DevSecOps landscape.
  twitter_description: Join us at the GitLab DevSecOps World Tour in Irvine where we’ll be showcasing a comprehensive overview of the evolving DevSecOps landscape.
  og_image: /nuxt-images/events/world-tour/cities/irvine-illustration.png
  twitter_image: /nuxt-images/events/world-tour/cities/irvine-illustration.png
  time_zone: Pacific Time (PT)
  hero:
    breadcrumbs:
      - title: DevSecOps World Tour
        href: /events/devsecops-world-tour/
      - title: Irvine
    header: Irvine
    subtitle: September 21, 2023
    location: |
      Avenue of the Arts Costa Mesa
      3350 Avenue of the Arts
      Costa Mesa, CA 92626
    image:
      src: /nuxt-images/events/world-tour/cities/irvine-illustration.png
    button:
      text: Register for Irvine
      href: '#registration-form'
      variant: secondary
      icon:
        name: arrow-down
        variant: product
        size: sm
  agenda:
    - time: 9:30 am
      name: Registration & Breakfast
      speakers:
    - time: 10:00 am
      name: |
        Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps
      speakers:
        - name: Ashley Kramer
          title: Chief Marketing and Strategy Officer
          company: GitLab
          image:
            src:  /nuxt-images/events/world-tour/speakers/ashley-kramer.jpg
          biography:
            text: |
              Ashley Kramer is GitLab’s Chief Marketing and Strategy Officer. Ashley leverages her leadership experience in marketing, product, and technology to position GitLab as the leading DevSecOps platform. She also leads the strategy for product-led growth and code contribution to the GitLab platform.

              Prior to GitLab, Ashley was CPO and CMO of Sisense and has held several leadership roles, including SVP of Product at Alteryx and Head of Cloud at Tableau, as well as marketing, product, and engineering leadership roles at Amazon, Oracle, and NASA.
      description:
        title: 'Welcome Keynote: GitLab DevSecOps World Tour - The Evolution of DevSecOps'
        text: |
          Join us at the GitLab DevSecOps World Tour in Irvine where we’ll be showcasing a comprehensive overview of the evolving DevSecOps landscape. Learn how Artificial Intelligence (AI) is set to impact all teams across the SDLC and how we envision organizations confidently securing their software supply chain while staying laser focused on delivering business value.
    - time: 10:30 am
      name: Next up for The DevSecOps Platform
      speakers:
        - name: Justin Farris
          title: Senior Director, Product
          company: GitLab
          image:
            src: /nuxt-images/events/world-tour/speakers/Justin Farris.jpeg
          biography:
            text: Justin Farris is currently the Senior Director of Product at GitLab. With a background in Product Management, Strategy and Growth across numerous verticles. Justin brings a wealth of knowledge to help teams grow, scale & succeed. Prior to his role at GitLab, Justin served as the Director of Product at Zillow overseeing Growth teams.

      description:
          text: |
            Teams are facing more pressure than ever to deliver software faster — and software development processes are undergoing transformative changes. With recent AI innovations, we see teams developing, securing and deploying software very differently. Join us to hear how our vision for GitLab is set to change how teams function across the entire SDLC, followed by a deep dive into our latest innovations where you will learn how to keep security at the forefront powered by intelligent automations.
    - time: 11:15 am
      name: Break
    - time: 11:30 am
      name: Customer Spotlight - coming soon
      description:
        text: ''
    - time: 12:15 pm
      name: Lunch & Networking
    - time: 1:30 pm
      name: |
        **Workshop:** How to optimize your DevSecOps workflow by focusing on Value Streams
      speakers:
        - name: Brian Henzelmann
          title: Manager, Solutions Architecture
          company: GitLab
          biography:
            text: After 6 years enabling DevOps and coaching quality engineering for large enterprises across the Midwest, Brian Henzelmann now leads Enterprise Solutions Architecture for US West at GitLab. Coupling this with his over 10 years of hands-on experience as a full stack engineer delivering cloud native software gives him a unique vantage point for driving improved efficiencies in complex software value streams.
          image:
            src: /nuxt-images/events/world-tour/speakers/Brian Henzelmann.jpg
        - name: David	Astor
          title: Solutions Architect Manager
          company: GitLab
          biography:
            text: |
              David Astor is a Solutions Architect Manager with GitLab.  He’s worked with numerous organizations of all sizes to assist them in simplifying their DevOps workflows. He has a great passion for learning new things and loves getting to the “wow!” moment with the folks he helps.
              He’s a listener, a story teller, an agilist, a part time runner, an avid college football fan and loyal husband and friend. He’s a father to an incredible daughter and general purveyor of all manner of useless trivia.
              He’s a huge proponent of breaking down walls and getting people to work together. Oh, and dad jokes.
          image:
            src: /nuxt-images/events/world-tour/speakers/David Astor.png
        - name: Jonathan Fullam
          title: VP of Global Solutions Architecture
          company: GitLab
          biography:
            text: Currently, Jonathan leads a global team of value delivery and technical experts as Vice President of Solutions Architecture at GitLab, focusing on helping enterprises drive their business-oriented outcomes by realizing new capabilities and efficiencies within their operating model, technology strategy, and software delivery and management processes.
          image:
            src: /nuxt-images/events/world-tour/speakers/Jonathan Fullam.jpeg
        - name: Reshmi Krishna
          title: Director, Enterprise Solutions Architecture
          company: GitLab
          biography:
            text: Reshmi has extensive experience in being an engineer, solution architect & has held leadership positions for multiple years. Reshmi leads with empathy, follows servant leadership, values selling strategies & loves to dive deeper into technical details. Reshmi has expertise in areas such as cloud-native technologies, digital transformation, DevOps, value stream mapping.Outside of work, Reshmi loves to represent solution architecture in various conferences, meetups & podcasts. Reshmi recently started writing her own blogs encouraging folks to become a solution architect with the hopes that one day, she will no longer be a minority in the field.
          image:
            src: /nuxt-images/events/world-tour/speakers/Reshmi Krishna.jpg
      description:
        text: |
          If your team is looking to optimize your DevOps practices, join the DevOps Value Stream experts for valuable insights and actionable strategies. This interactive workshop will focus on discovery, identification and removal of blockers to drive visibility and continuous improvement. Dive deeper into Value Stream Assessments (VSA) and uncover areas of improvement in the SDLC. You will learn how you can accelerate value streams for faster business value realization. This interactive exercise will also provide practical experience for you to apply these VSA methodologies to your teams to identify key areas for improvement. At the end of this session, you will have a solid understanding of VSA principles, practical tools and techniques to set you ahead on the path to improving your DevOps processes.
    - time: 3:00 pm
      name: Break
    - time: 3:15 pm
      name: |
        Partner Spotlight: NextLink Labs
      speakers:
        - name: 'Sponsor Session with GitLab Partner, NextLink Labs'
          title: ' '
          company: ''
          image:
            src: /nuxt-images/events/world-tour/sponsors/nextlink.png
            contain: true
          biography:
            text:
      description:
        text: |
          Join us and hear from NextLink Labs on how they are uniquely positioned to power cloud innovation and deliver customer value alongside business transformation. Learn how, with the combined strength of GitLab & NextLink Labs customers are achieving their business goals with an accelerated pace of innovation and faster time to market.

    - time: 3:40 pm
      name: |
        **Roundtable discussion:** The human element of DevSecOps
      description:
          text: |
            ‘Technology isn’t the hard problem anymore, culture seems to be the chief problem now.’- Is this a statement that you have come across?

            We invite you to discuss with leaders the role and impact of people on DevOps processes. Dig deeper into why communication, collaboration and empathy play a critical role in effective DevOps strategies. What are the strategies that work while introducing DevOps into businesses. Does it work if you find the core problem the organization faces and start there? Is education on agile a great place to start when thinking about building a DevOps culture? Join us and weigh in on the best practices to foster teamwork, promote accountability and  develop a human-centered design for DevOps processes.

    - time: 4:10 pm
      name: Closing Remarks
    - time: 4:30 pm
      name: Snacks + Networking
  sponsors:
    - img: /nuxt-images/events/world-tour/sponsors/nextlink.png
      alt: NextLink Labs
  form:
    multi_step: true
    header: Register for DevSecOps World Tour in Irvine
    confirmation:
      - Thank you for registering!
      - Please check your email for a confirmation email.
    form_id: '3703'
  conduct_banner:
    header: Code of conduct
    text: GitLab is committed to providing a safe and welcoming experience for every attendee at all of our events whether they are virtual or onsite. Please review our [code of conduct](/company/culture/ecoc/) to ensure DevSecOps World Tour is a friendly, inclusive, and comfortable environment for all participants.
