---
  title: What does infrastructure as code mean?
  description:  Infrastructure as code (IaC) automates the provisioning of IT infrastructure by using configuration files. Automation leads to more efficient development, increased consistency, and faster time to market.
  date_published: 2022-02-09
  date_modified: 2023-04-03
  topics_header:
      data:
        title: What does infrastructure as code mean?
        block:
            - text: |
                  Infrastructure as code (IaC) automates the provisioning of IT infrastructure by using configuration files. Automation leads to more efficient development, increased consistency, and faster time to market.
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: Gitops
      href: /topics/gitops/
      data_ga_name: topics gitops
      data_ga_location: breadcrumb
    - title: Infrastructure as code
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: What is Infrastructure as Code (IaC)
          href: "#what-is-infrastructure-as-code-ia-c"
          data_ga_name: what is infrastructure as code iac
          data_ga_location: side-navigation
          variant: primary
        - text: What are the benefits of infrastructure as code?
          href: "#what-are-the-benefits-of-infrastructure-as-code"
          data_ga_name: how do teams put git ops into practice
          data_ga_location: side-navigation
    hyperlinks:
      text: ''
      data: []
    content:
      - name: 'topics-copy-block'
        data:
          header: What is Infrastructure as Code (IaC)
          column_size: 10
          blocks:
              - image:
                  image_url: /nuxt-images/topics/gitops-topic.png
                  top: true
                  alt:
                text: |
                  Managing IT infrastructure can be a tedious task, especially since it often involves manual processes that require configuring physical servers. Configurations have to be on the perfect setting for applications and operating systems or teams won’t be able to deploy. Looking for a simpler solution, teams began to create infrastructure setup as code to manage and provision infrastructure. > Infrastructure as Code (IaC) automates the provisioning of IT infrastructure by using configuration files. Automation leads to more efficient development, increased consistency, and faster time to market.

                  As a DevOps practice, Infrastructure as Code enables teams to quickly version infrastructure in a way that improves consistency across machines to reduce friction when deploying. IaC travels through the same path as application code, including [continuous integration (CI)](/stages-devops-lifecycle/continuous-integration/) and continuous delivery (CD), version control, and testing.

                  ### What problems does IaC solve?
                  Lack of visibility into performance results from the inability to monitor each step of the process. When a problem arises, teams struggle to identify where there’s a failure in the infrastructure.

                  **High costs associated with managing infrastructure** stem from the high number of individuals responsible for managing each stage of the process and the physical space needed for the servers. Specialized team members are required to handle specific tasks, tools are needed to increase communication, and space is needed to house the physical infrastructure.

                  **Inconsistency across infrastructure** is a common occurrence, since everyone manually deploys configurations. These unique environments can’t be reproduced automatically and can’t be tracked easily.

                  **Unpredictable scalability, reliability, and availability** accompanies manual configurations, resulting in downtimes. System administrators are often limited in how they configure servers to manage increases in load, and they can struggle to keep up with sudden changes in access.
      - name: 'topics-copy-block'
        data:
          column_size: 10
          header: What are the benefits of infrastructure as code?
          blocks:
              - text: |
                  The impact of infrastructure as code can be seen across a business - from development to cost - and felt by teams across the software development lifecycle.

                  ### Development is faster and more efficient
                  By running a script, teams can quickly set up infrastructure for every phase of the development lifecycle and environment, including development, production, staging, and testing. IaC supports teams from across the lifecycle, enabling all teams to benefit from accelerated processes - from developers who can provision sandboxes, QA that can create high fidelity test environments, and security which can build tests to identify vulnerabilities. Automation accelerates software delivery and ensures that the development lifecycle is an efficient process.

                  ### DevOps practices strengthen infrastructure code
                  With infrastructure written as code, DevOps teams can test applications in production environments earlier in the software development lifecycle and quickly provision test environments as needed. Code goes through the same version control process and CI/CD pipeline as application code to be validated and tested. Using DevOps practices, infrastructure code creates stable environments that can be quickly provisioned at scale. Teams collaborated to develop strong DevOps practices to support infrastructure reliably.

                  ### Consistency reduces configuration drift
                  Historically, teams have manually configured infrastructure, but snowflake environments can develop when individuals create ad-hoc configuration changes. Manual infrastructure management results in discrepancies across development, testing, and deployment environments, leading to security vulnerabilities and deployment problems. If an application must be developed according to specific compliance standards, snowflake environments risk meeting these strict regulatory requirements. With IaC, the same environment is provisioned each time, creating a single source of truth and eliminating the problems associated with manual configuration. Teams provision servers and applications to align with specific policies, ensuring that business practices are followed.

                  ### Configuration increases accountability
                  Because infrastructure configuration exists as code files, teams can easily put it in source control to edit and distribute like other source code files. Teams now have traceability and can identify changes and authors and revert if necessary.

                  ### Automation reduces costs
                  When teams combine cloud computing with IaC, costs decrease, because teams no longer require individuals to monitor hardware or rent space to house the machines. With IaC, teams benefit from a cloud computing’s consumption-based pricing model.
  components:
    - name: solutions-resource-cards
      data:
        title: Related Resources
        column_size: 4
        grouped: true
        cards:
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: "Blogs"
            header:  Why GitOps should be the workflow of choice
            link_text: "Learn more"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-devops.png"
            href: "/blog/2020/04/17/why-gitops-should-be-workflow-of-choice/"
            data_ga_name:  Why GitOps should be the workflow of choice
            data_ga_location: resource cards

          - header: Download the beginner’s guide to GitOps
            link_text: Learn more
            image: "/nuxt-images/resources/resources_2.jpeg"
            event_type: Books and eBooks
            icon:
              name: book
              variant: marketing
              alt: Book Icon
            href: https://page.gitlab.com/resources-ebook-beginner-guide-gitops.html
            data_ga_name: Download the beginner’s guide to GitOps
            data_ga_location: resources

          - header: Learn how GitLab simplifies infrastructure as code
            link_text: Learn more
            image: "/nuxt-images/resources/resources_10.jpeg"
            event_type: Articles
            icon:
              name: article
              variant: marketing
              alt: Book Icon
            href: /solutions/gitops/
            data_ga_name: Learn how GitLab simplifies infrastructure as code
            data_ga_location: resources
          - header: How teams use GitLab and Terraform for infrastructure as code
            link_text: Learn more
            image: "/nuxt-images/resources/resources_3.jpg"
            event_type: Articles
            icon:
              name: article
              variant: marketing
              alt: Book Icon
            href: /topics/gitops/gitlab-enables-infrastructure-as-code/
            data_ga_name: How teams use GitLab and Terraform for infrastructure as code
            data_ga_location: resources
