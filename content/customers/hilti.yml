---
  data:
    customer: Hilti
    customer_logo: /nuxt-images/logos/hilti_logo_1.svg
    heading: How CI/CD and robust security scanning accelerated Hilti’s SDLC
    key_benefits:
      - label: Streamlined code management
        icon: bulb-bolt
      - label: Improved delivery time
        icon: speed-alt
      - label: CI/CD capabilities
        icon: continuous-integration
    header_image: /nuxt-images/blogimages/hilti_cover_image.jpg
    customer_industry: Manufacturing
    customer_employee_count: 30,000
    customer_location: Schaan, Liechtenstein (HQ)
    customer_solution: |
      [GitLab Self Managed Ultimate](/pricing/ultimate/){data-ga-name="ultimate solution" data-ga-location="customers hero"}
    sticky_benefits:
      - stat: 400%
        label: increase in code checks
      - stat: 50% 
        label: shorter feedback loops
      - stat: 12x
        label: faster deployment time
    blurb: GitLab’s SCM, seamless CI/CD, and exceptional security scanning empowered Hilti to bring code in-house.
    introduction: |
      Hilti expanded its software capabilities and adopted GitLab to bring code in-house with SCM, CI/CD, and security scanning.
    quotes:
      - text: |
          GitLab is bundled together like a suite and then ships with a very sophisticated installer. And then it somehow works. This is very nice if you're a company which just wants to get it up and running.
        author: Daniel Widerin
        author_role: Head of Software Delivery, Hilti
        author_company: Hilti
    content:
      - title: 'Global construction services provider'
        description: |
          Hilti is a world leader in the design and manufacturing of cutting-edge technologies, software, and services for the professional construction industry. One sector of [Hilti](https://www.hilti.group/) specifically focuses on business unit tool solutions. This team creates software for customers in the area of software development tools which meets governance, risk, and compliance regulations. Hilti ensures that the correct procedures are in place to adhere with regulatory compliances across different regions.
      
      - title: Enrich software capabilities, security, and SCM
        description: |
          About two years ago, Hilti was looking for a software platform to rebuild their projects. They had previously outsourced one of their software development projects to an external vendor as the software capability could not be 100% managed in-house. The source code was owned by a joint venture which used GitHub. Hilti owned the majority of the joint venture, but it was not hosting source code in-house. There wasn’t any internal CI/CD and also the teams didn’t perform security testing according to the highest standards. This situation was challenging because the software teams wanted full visibility and management of their code.
      
          Hilti’s goal was to move software development in-house to enable the engineering and architecture teams to conduct proper reviews, truly collaborate, and to share best practices with other teams. As Hilti wanted a solution that adheres to the highest standards, the ideal tool would need to be easy to onboard, be intuitive, and offer seamless integration. “We wanted to bring a tool on to our own premises so that we have it under our control, and also have it in real time. That was really a big step forward,” said Raphael Hauser, Head of Governance at Hilti. Security scanning was high on the priority list. Hilti has between 10 to 15 distributed teams working in parallel on large-scale solutions at any given time globally. Security needs to be under control and aggregated so that by the time a software release is ready, vulnerabilities are visible ahead of time. Hilti needed a tool with powerful and reliable security capabilities.

          Development and test teams previously found themselves in “reactive mode” when catching bugs. A tool that offers a way to find vulnerabilities within the pipeline would be more efficient, increase workflow speed, and empower developers. “I want to be sure, once we release a package to production, that we're not bringing in any packages of code that are eventually creating a risk for Hilti; exposing source code is an access security problem, not a code scanning problem,” Hauser added.

      - title: Bringing security and code in-house
        description: |
          After a review and working with various tools, GitLab was adopted for its ease of integration, SCM capabilities, and comprehensive security scanning. GitLab delivered the capabilities to maintain high software performance standards and to quickly provide multiple types of in-depth scanning. Hilti is using GitLab’s static and dynamic application security testing (SAST and DAST, respectively), along with container scanning, dependency scanning, secret detection, and license compliance. “GitLab is far ahead of its competitors and provides one product which offers an easy-to-set-up, easy-to-start product with all these capabilities integrated,” said Daniel Widerin, Head of Software Delivery.
            
          Hilti has compliance regulations that they must follow, including license reviewing, application testing, and source code access. Hilti opted for GitLab Ultimate in order to use the compliance and security scanning. “From a risk point of view, the key factor was that we can now control much tighter and much closer who really has access to the source code, who is managing the source code, and the current state in regard to security and IP compliance,” Hauser said.

          With GitLab, Hilti now has full access to their source code and is able to manage it properly. Owning their own code reduces the risk of any source code leakage and increases the level of code change capabilities. “It has given me much more insight into what is actually happening within the code and doing that in real time. It has also sped up my way of giving approvals in regard to security, code security, and the IP compliance in order to still comply with the faster pace of delivery,” Hauser said.

      - title: Secure code, end-to-end visibility, and faster deployments
        description: |
          Hilti’s engineering and architecture teams now use GitLab for SCM, CI/CD, and security dashboards that are compatible with their technology stack. With GitLab, they were able to build software in-house and at a faster rate than if they had used a complicated set of tools. The ease of integration allows the teams to work with Jira, Docker, and Amazon Web Services (AWS). All services integrated with GitLab, including build artifacts and runners, are running on AWS and deployed to a Kubernetes cluster.
                
          “GitLab has done a really great job with the source code to bring feedback directly after you have opened the merge request or after you have done a comment or a push,” said Widerin. “I mean, you basically don’t have to develop all these things on your own. GitLab is bundled together like a suite and then ships with a very sophisticated installer. And then it somehow works. This is very nice if you're a company which just wants to get it up and running.” With GitLab, feedback loops have shortened by 50%, from 6 days to 3 days, supporting greater efficiency and collaboration.

          Team members appreciate that the pipeline is directly integrated into the source code and they’re able to get immediate feedback from the merge request including security scan results. “People really like that they have a center point where they can log in and see all the different microservices and components while they are working, even with mobile apps and web UI,” Widerin added. Code checks have increased significantly from six times every three months to twice a week, thereby maintaining high quality. 

          Deployment speeds have increased because now development and test teams own the code and can see when there are vulnerabilities ahead of time. Deployment times have decreased from an average of three hours to just 15 minutes with GitLab. They now have clear guidance on what they accept for any release with regard to severity of vulnerabilities within the code. “We are faster to remedy critical findings and the teams get a bit more stability because they do not have to do a firefight before the release ... it helps us to give them the overview of where they stand so they don't have to rework after the sprint is complete,” Hauser said.
