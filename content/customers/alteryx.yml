---
  data:
    title: Alteryx
    description: GitLab gives Alteryx visibility to increase build speed
    og_title: Alteryx
    twitter_description: GitLab gives Alteryx visibility to increase build speed
    og_description: GitLab gives Alteryx visibility to increase build speed
    og_image: /nuxt-images/blogimages/alteryx_case_study.jpg
    twitter_image: /nuxt-images/blogimages/alteryx_case_study.jpg

    customer: Alteryx
    customer_logo: /nuxt-images/logos/alteryx.svg
    heading: Removing multi-tool barriers to achieve 14 builds a day at Alteryx
    key_benefits:
      - label: Faster build times
        icon: accelerate-thin
      - label: Increased visibility
        icon: visibility
    header_image: /nuxt-images/blogimages/alteryx_case_study.jpg
    customer_industry: Technology
    customer_employee_count: 900+
    customer_location: Irvine, California
    customer_solution: |
      [GitLab Ultimate](/pricing/ultimate/){data-ga-name="ultimate solution" data-ga-location="customers hero"}
    sticky_benefits:
      - label: commits since 2016
        stat: 10,464
      - label: daily releases
        stat: 9x
      - label: projects using GitLab
        stat: 769
    blurb: Data science and analytics leader Alteryx needed a new toolset to modernize their software development lifecycle.
    introduction: |
      With GitLab, Alteryx has been able to improve the development experience and accelerate the organization's pace of deployment — all with a single tool.
    content:
      - title: Achieving the full potential of data
        description: |
          Alteryx offers an end-to-end analytics platform that empowers data analysts and scientists to break data barriers, deliver insights, and get to the answer faster. Organizations all over the world rely on Alteryx daily to deliver actionable insights. The end-to-end Alteryx analytics platform enables analysts and data scientists to discover, share and prep data, perform analysis, and deploy and manage analytic models.
      - title: Modernizing to improve the development experience
        description: |
          Alteryx knew their legacy system of using Subversion with Jenkins was not the optimal toolset to accomplish the goal of improving their software development lifecycle. At that time, the repository was more than 500 gigabytes and was filled with legacy code, making it difficult to manage.

          They wanted to modernize the development experience to improve deployment and build pace. To achieve this they moved to Git, established an enterprise source code repository tool, and improved their continuous integration and delivery (CI/CD). They evaluated three tools, GitHub, Perforce, and GitLab, in this process.
      - title: Evaluating how to best achieve the goal
        description: |
          GitLab allowed Alteryx to truly have everything in one place and within one tool — one place for CI, CD, source code management, code reviews, and security scanning. Having everything in one place also made it easier for them to scale because teams can just be added to GitLab and have all the things that typically take them 3 or 4 months to get set up.
      - title: Success with a single application and double the anticipated users
        description: |
          Alteryx began with the Community Edition of GitLab in 2016 but quickly moved to the Enterprise Edition so they could scale. Teams soon realized the value of a single application to manage their work and they more than doubled their planned number of users in the first six months. Alteryx staff are inspired to use it because the CI is baked right into the repository, allowing developers to actively participate in the DevOps process.
      - title: Accelerating deployment with everything in one place
        description: |
          GitLab has allowed Alteryx to have code reviews, source control, continuous integration, and continuous deployment all tied together and speaking the same language. This capability allows each commit or merge request to get a code review, which wasn’t previously happening. That code review is tied to a deployment, which is tied to a URL and so on.

          The team took a build that was running legacy systems and moved it to GitLab. This build took 3 hours on the Jenkins machine and it took 30 minutes to run on GitLab after it was going. Engineers can actually look at the build and understand what’s going on; they’re able to debug it and make it successful.
