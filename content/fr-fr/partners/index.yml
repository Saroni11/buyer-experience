---
  title: Partenaires de l'Alliance Cloud, plateformes et technologies
  description: Découvrez les partenaires de GitLab, qui ils sont, quels services ils fournissent et comment vous pouvez devenir partenaire.
  components:
    - name: 'hero'
      data:
        title: Programme de partenariat GitLab
        text: « Si tu veux aller vite, marche seul. mais si tu veux aller loin, marchons ensemble. » - Proverbe
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: S'inscrire maintenant
          url: https://partners.gitlab.com/English/register_email.aspx
          data_ga_name: apply today
          data_ga_location: header
        secondary_btn:
          text: Trouver un partenaire agréé
          url: https://partners.gitlab.com/English/directory/search?f0=Partner+Certifications&f0v0=Professional+Services&l=United+States&Adv=none
          data_ga_name: find a channel partner
          data_ga_location: header
        tertiary_btn:
          text: Trouver un partenaire d'alliance
          url: /partners/technology-partners/
          data_ga_name: find an alliance partner
          data_ga_location: header
        image:
          url: /nuxt-images/solutions/infinity-icon-cropped.svg
          alt: "Image\_: partenaires gitlab"
    - name: 'copy-block'
      data:
        header: Présentation
        anchor_id: Overview
        top_margin: slp-mt-md-64
        column_size: 9
        blocks:
          - text: |
              GitLab s'associe à un écosystème mondial de partenaires leaders pour soutenir les besoins croissants de nos clients en matière de DevSecOps et de transformation numérique. Grâce à notre engagement en faveur d'une collaboration ouverte, nous avons créé un écosystème de partenaires proposant des intégrations complètes, une assistance et des services tout au long du cycle de vie du logiciel.
    - name: 'copy-block'
      data:
        header: Partenaires agréés, de revente, d'intégration et de formation
        anchor_id: channel-resell-integration-and-training-partners
        column_size: 9
        blocks:
          - text: |
              Les partenaires de vente et d'intégration de GitLab dans le monde entier aident les clients à atteindre leurs objectifs techniques et commerciaux dans le cadre de la transformation numérique. Nous nous associons aux principaux fournisseurs de solutions (intégrateurs de systèmes, partenaires de plateformes Cloud, revendeurs, distributeurs, fournisseurs de services gérés et partenaires de l'écosystème) pour maximiser la valeur que les clients tirent de l'adoption de GitLab en tant que plateforme DevSecOps tout-en-un. Nos partenaires agréés sont autorisés à revendre ou à acheter des licences pour le compte d'utilisateurs finaux et/ou à fournir des services de déploiement, d'intégration, d'optimisation, de gestion et de formation aux clients de GitLab.

              Ces solutions sont un élément essentiel de la mission de GitLab qui consiste à permettre à nos clients de profiter d'expériences modernes basées sur les logiciels. En collaboration avec nos partenaires, nous aidons les entreprises et les organisations de toutes tailles et de tous secteurs verticaux, dans le monde entier, à mener la transformation numérique nécessaire pour opérer plus efficacement tout en offrant une excellente expérience client.
    - name: 'partners-showcase'
      data:
        padding: 'slp-my-32 slp-my-md-48'
        items:
          - image:
              src: /nuxt-images/partners/badges/gitlab-select-partner-badge.svg
              alt: badge partenaire select
            title: Revendeurs agréés Select
            text: Les partenaires Select sont des partenaires qui investissent davantage dans l'expertise GitLab, développent des pratiques de services autour de GitLab et sont censés générer des revenus récurrents plus importants pour les produits GitLab. La participation au programme Select Partner se fait uniquement sur invitation de GitLab.
          - image:
              src: /nuxt-images/partners/badges/gitlab-open-partner-badge.svg
              alt: badge partenaire open
            title: Revendeurs agréés Open
            text: Notre parcours Open est accessible à tout partenaire qui remplit les conditions minimales et qui identifiera ou soutiendra les opportunités de vente de GitLab. Les partenaires Open de GitLab peuvent ou non être des partenaires commerciaux et peuvent bénéficier de remises sur les produits ou recevoir des commissions de parrainage. Les revendeurs, les intégrateurs et les autres partenaires de vente et de services rejoignent le programme dans le cadre du parcours Open.
          - image:
              src: /nuxt-images/partners/badges/gitlab-professional-services-partner-badge.svg
              alt: badge partenaire de services professionnels
            title: Intégrateurs de systèmes et services professionnels mondiaux et régionaux
            text: Soutenez votre mise en œuvre et votre adoption avec des services de déploiement, d'intégration et d'optimisation. Ces partenaires peuvent également revendre le logiciel GitLab.
    - name: 'copy-block'
      data:
        no_decoration: true
        column_size: 9
        blocks:
          - text: |
              GitLab propose également des certifications pour les partenaires qui leur permettent de développer une expertise plus approfondie de GitLab.  La certification Expert des Services professionnels GitLab leur permet de se différencier avec des offres de services uniques et de favoriser l'adoption de la plateforme GitLab.  En outre, les Partenaires de formation certifiés GitLab sont en mesure de proposer des formations GitLab ou des formations personnalisées pour aider les clients à développer une plus grande expertise dans l'utilisation de GitLab.
            link:
              text: Trouver un partenaire agréé
              url: https://partners.gitlab.com/English/directory/search?f0=Partner+Certifications&f0v0=Professional+Services&l=United+States&Adv=none
              data_ga_name: find a partner

    - name: 'copy-block'
      data:
        header: Partenaires de l'Alliance Cloud, plateformes et technologies
        anchor_id: cloud-platform-and-technology-alliance-partners
        column_size: 9
        blocks:
          - text: |
              Nous collaborons avec des fournisseurs de Cloud et de technologie leaders dans tous les grands secteurs d'activité afin de proposer la meilleure plateforme DevSecOps moderne qui soit.  Nos partenaires intègrent GitLab pour fournir des solutions DevSecOps personnalisées dans tous les secteurs d'activité et tous les cas d'utilisation. Ils jouent un rôle essentiel dans la mission de GitLab : permettre à nos clients de bénéficier d'expériences logicielles modernes et faire en sorte que « tout le monde puisse contribuer » grâce à un écosystème de partenaires solide et prospère qui cultive l'innovation et stimule la transformation. Ensemble, nous permettons à nos entreprises clientes de mener la transformation numérique nécessaire pour rivaliser efficacement sur le marché d'aujourd'hui.
            link:
              text: Trouver un partenaire d'alliance
              url: /partners/technology-partners/
              data_ga_name: find an alliance partner
    - name: 'partners-showcase'
      data:
        padding: 'slp-mt-32 slp-mt-md-48'
        clickable: true
        items:
          - image:
              src: /nuxt-images/partners/badges/gitlab-cloud-partner-badge.svg
              alt: badge partenaire cloud
            text: Les fournisseurs de services Cloud et les hyperscalers qui fournissent des services de cloud computing public et des places de marché logicielles où les clients peuvent se procurer et déployer GitLab. Nous avons simplifié les déploiements de GitLab en nous associant à des fournisseur de services cloud (FSC) de premier plan afin de fournir plus rapidement de meilleurs logiciels. Nos intégrations natives dans le Cloud offrent un accès direct aux environnements auxquels les développeurs font le plus confiance.
            url:
              href: /partners/technology-partners/#cloud-partners
              ga:
                data_ga_name: cloud partner
                da_ga_lcoation: body
          - image:
              src: /nuxt-images/partners/badges/gitlab-platform-partner-badge.svg
              alt: badge partenaire de plateforme
            text: Partenaires matériels et logiciels qui fournissent des plateformes d'application Cloud/cloud-native dans le Cloud modernes qui augmentent la modularité et l'extensibilité de GitLab à travers les entreprises et les architectures.
            url:
              href: /partners/technology-partners/#platform-partner
              ga:
                data_ga_name: platform partner
                da_ga_lcoation: body
          - image:
              src: /nuxt-images/partners/badges/gitlab-technology-partner-badge.svg
              alt: badge partenaire technologique
            text: Éditeurs de logiciels indépendants (ISV) avec des technologies complémentaires qui s'intègrent/interopèrent de manière transparente avec GitLab pour des solutions clients plus complètes.
            url:
              href: /partners/technology-partners/#technology-partners
              ga:
                data_ga_name: technology partner
                da_ga_lcoation: body
    - name: 'copy-block'
      data:
        header: Favoriser des partenariats fructueux
        anchor_id: driving-successful-partnerships
        column_size: 9
        blocks:
          - text: |
              __« Si tu veux aller vite, marche seul. mais si tu veux aller loin, marchons ensemble. »__

              Ce proverbe illustre parfaitement notre engagement en faveur d'une réussite mutuelle. GitLab a développé un ensemble solide de programmes d'habilitation des partenaires, de formation et de programmes commerciaux pour permettre à notre écosystème de partenaires et de clients de tirer tous les avantages des investissements DevSecOps et de la transformation numérique.
            link:
              url: /partners/benefits/
              text: Voir les avantages pour les partenaires GitLab
              data_ga_name: see gitlab partner benefits
    - name: 'quotes-carousel'
      data:
        no_background: true
        quotes:
          - main_img:
              url: /nuxt-images/partners/headshots/HeadshotMichaelMcBride.png
              alt: Portrait de Michael McBride, Chief Revenue Officer chez GitLab
            quote: |
              « Nous voyons une énorme opportunité d'aider plus de 100 000 entreprises à consolider leurs nombreux outils DevSecOps et à adopter une approche de plateforme. Nous travaillons de concert avec nos partenaires pour accélérer la réussite des clients avec la plateforme DevSecOps de GitLab en fournissant des services qui transforment les outils, la culture et les processus des entreprises. »
            author: Michael McBride
            job: Chief Revenue Officer chez GitLab
    - name: 'resources'
      data:
        header: Solutions partenaires
        links:
          - name: Services professionnels
            link: /services/
            description: |
              Services de mise en œuvre, de déploiement, d'intégration et d'optimisation
            ga:
              name: professional services
              location: body
          - name: Services gérés (MSP)
            link: https://partners.gitlab.com/English/directory/search?f0=Services+Offered&f0v0=Managed+%2F+Hosted+Services&Adv=none
            description: |
              Service d'hébergement GitLab géré clé en main pour les besoins souverains
            ga:
              name: managed (msp) services
              location: body
          - name: Services de formation
            link: https://partners.gitlab.com/English/directory/search?f0=Services+Offered&f0v0=Training&Adv=none
            description: |
              Certification formelle et cours de formation personnalisés
            ga:
              name: training services
              location: body
          - name: Certifications partenaires
            link: /handbook/resellers/services/
            description: |
              Formez-vous professionnellement et devenez un partenaire GitLab Resell, PS, MSP et de formation
            ga:
              name: partner certifications
              location: body
          - name: Portail des partenaires
            link: https://partners.gitlab.com/English/
            description: |
              Accédez à notre portail des partenaires à accès restreint pour des ressources uniques, des solutions et l'enregistrement de transactions
            ga:
              name: partner portal
              location: body
          - name: Mettre à niveau
            link: https://partners.gitlab.com/English/?ReturnUrl=/prm/English/c/Training
            description: |
              Apprenez de nouvelles compétences et développez vos opportunités professionnelles (l'accès au portail des partenaires est nécessaire)
            ga:
              name: level up
              location: body
          - name: Formation, certifications et habilitation des partenaires
            link: /handbook/resellers/training/
            description: |
              Formation et habilitation basées sur le rôle pour développer vos compétences et votre expertise GitLab
            ga:
              name: partner training, certifications, and enablement
              location: body
          - name: Certifications professionnelles
            link: /learn/certifications/public/
            description: |
              Certifications accessibles au public pour les utilisateurs de GitLab et les experts professionnels
            ga:
              name: professional certifications
              location: body
          - name: Intégration de la technologie et intégration
            link: /partners/technology-partners/integrate/
            description: |
              De nouveaux partenaires peuvent créer des solutions intégrées/interopérables avec GitLab
            ga:
              name: technology integration & on-boarding
              location: body
    - name: 'copy-block'
      data:
        header: Rencontrez nos partenaires en vedette
        anchor_id: meet-our-featured-partners
        column_size: 9
        blocks:
          - text: |
              Nous avons simplifié la prise en main de GitLab en nous associant à des partenaires de premier plan dans les domaines du Cloud, du DevSecOps, de la technologie, des solutions, de la revente et de la formation pour vous aider à fournir plus rapidement de meilleurs logiciels. Nos intégrations GitLab constituent un accès direct aux environnements et aux outils auxquels les développeurs font le plus confiance.
    - name: 'intro'
      data:
        as_cards: true
        logos:
          - name: VMware Tanzu
            image: /nuxt-images/partners/vmware/logo-vmware-tanzu-square.jpg
            url: /partners/technology-partners/vmware-tanzu/
            aria_label: Lien vers l'étude de cas VMware Tanzu partner
          - name: IBM
            image: /nuxt-images/partners/ibm/ibm.png
            url: /partners/technology-partners/ibm/
            aria_label: Lien vers l'étude de cas IBM partner
          - name: Redhat
            image: /nuxt-images/partners/redhat/redhat_logo.svg
            url: /partners/technology-partners/redhat/
            aria_label: Lien vers l'étude de cas Redhat partner
          - name: Hashicorp
            image: /nuxt-images/partners/hashicorp/hashicorp.svg
            url: /partners/technology-partners/hashicorp/
            aria_label: Lien vers l'étude de cas Hashicorp partner
          - name: GCP
            image: /nuxt-images/partners/gcp/GCP.svg
            url: /partners/technology-partners/google-cloud-platform/
            aria_label: Lien vers l'étude de cas GCP partner
          - name: AWS
            image: /nuxt-images/partners/aws/aws-logo.svg
            url: /partners/technology-partners/aws/
            aria_label: Lien vers l'étude de cas AWS partner
