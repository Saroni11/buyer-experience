---
  title: Politique de cookies de GitLab
  description: Sur cette page, vous pouvez trouver des informations sur la politique de cookies de GitLab. Apprendre encore plus!
  side_menu:
    anchors:
      text: "Sur cette page"
      data:
        - text: Pourquoi utilisons-nous des cookies?
          href: "#why-do-we-use-cookies"
          data_ga_name: why do we use cookies
          data_ga_location: side-navigation
        - text: Qui paramètre les cookies sur GitLab?
          href: "#who-sets-cookies-on-gitlab"
          data_ga_name: who sets cookies on gitlab
          data_ga_location: side-navigation
        - text: Comment puis-je gérer mes cookies?
          href: "#how-do-i-manage-my-cookies"
          data_ga_name: how do i manage my cookies
          data_ga_location: side-navigation
        - text: Notifications « Interdire le suivi »
          href: "#do-not-track-signals"
          data_ga_name: do not track signals
          data_ga_location: side-navigation
        - text: Contrôle global de la confidentialité (Global Privacy Control en anglais)
          href: "#global-privacy-control"
          data_ga_name: global privacy control
          data_ga_location: side-navigation
        - text: Programmes d'autorégulation
          href: "#self-regulatory-programs"
          data_ga_name: self regulatory programs
          data_ga_location: side-navigation
        - text: Balises Web
          href: "#web-beacons"
          data_ga_name: web beacons
          data_ga_location: side-navigation
        - text: Identifiants de publicité mobile
          href: "#mobile-advertising-ids"
          data_ga_name: mobile advertising ids
          data_ga_location: side-navigation
        - text: Quels types de cookies utilisons-nous?
          href: "#what-types-of-cookies-do-we-use"
          data_ga_name: what types of cookies do we use
          data_ga_location: side-navigation
    hyperlinks:
      text: ''
      data: []
  hero_title: Politique de cookies de GitLab
  disclaimer:
    text: |
      Cette traduction est fournie à titre informatif uniquement.
    details: |
      En cas de divergence entre le texte anglais et cette traduction, la version anglaise prévaudra.
  copy: |
      La présente Politique en matière de cookies complète les informations contenues dans notre Déclaration de confidentialité et explique comment nous utilisons les cookies et les technologies associées pour gérer et fournir nos sites Web, produits et services GitLab, collectivement dénommés les « Services ».

      Les cookies sont de petits fichiers texte placés sur votre ordinateur. Les pixels sont de petites quantités de code sur une page Web ou dans un courriel qui permettent de fournir un contenu, tel qu'une image graphique sur une page Web.
  sections:
    - header: Pourquoi utilisons-nous des cookies?
      id: why-do-we-use-cookies
      text: |
        GitLab utilise des cookies pour :

        - Vous aider à accéder et à utiliser les Services ;
        - Comprendre comment les visiteurs utilisent et interagissent avec nos Services ;
        - Configurer vos préférences ;
        - Fournir une publicité pertinente basée sur les centres d'intérêt ;
        - Comprendre si vous avez ouvert un courriel et y avez donné suite ; et
        - Analyser et améliorer nos Services.
    - header: Qui paramètre les cookies sur GitLab?
      id: who-sets-cookies-on-gitlab
      text: |
        Les cookies sont parfois placés par GitLab, connus sous le nom de cookies propriétaires, et parfois des cookies tiers sont configurés par nos prestataires de services, tels que Google Analytics qui fournit des analyses et des publicités basées sur les centres d’intérêt. Lorsque des cookies tiers sont configurés par des prestataires de services, ils fournissent un service ou une fonction à GitLab, tout en réalisant les propres objectifs du prestataire de services. GitLab ne peut pas contrôler la façon dont les cookies tiers sont utilisés. Vous pouvez consulter les options de désactivation disponibles de Google Analytics, y compris le module du navigateur Google Analytics, ici : <https://tools.google.com/dlpage/gaoptout/>.
    - header: Comment puis-je gérer mes cookies?
      id: how-do-i-manage-my-cookies
      text: |
        GitLab a unifié son outil de gestion des cookies dans tous les domaines de GitLab, offrant aux utilisateurs un endroit central pour gérer leurs préférences en matière de cookies. À l'exception des cookies « strictement nécessaires » qui sont essentiels au fonctionnement des Services, vous serez en mesure d'accepter, de refuser ou d'ajuster vos préférences en matière de cookies pour toutes les autres catégories de cookies. Veuillez noter que la désactivation de certains cookies peut empêcher certaines fonctionnalités des Services de s'exécuter correctement. Cet outil est accessible en cliquant sur le **lien Préférences en matière de cookies** (intitulé **Ne pas vendre ou partager mes informations personnelles** pour les résidents de Californie) situé en bas de page de chaque site Web sous « Société ».

        La plupart des navigateurs Web vous permettent également de supprimer les cookies déjà placés, ce qui peut supprimer les paramètres et les préférences contrôlés par ces cookies, y compris les préférences en matière de publicité. Vous trouverez des instructions pour supprimer les cookies qui ont été créés dans le dossier des cookies de votre navigateur ici : <https://allaboutcookies.org/manage-cookies/>.
    - header: Notifications « Interdire le suivi »
      id: do-not-track-signals
      text: |
        L'option « Interdire le suivi » (« Do Not Track » en anglais, ou DNT) est une préférence en matière de confidentialité que vous pouvez configurer dans votre navigateur Web pour indiquer que vous ne souhaitez pas être suivi. GitLab répond aux notifications DNT et respecte l'expression des préférences de l'utilisateur en matière de suivi pour les événements côté client.
    - header: Contrôle global de la confidentialité (Global Privacy Control en anglais)
      id: global-privacy-control
      text: |
        À l'instar des notifications DNT, le Contrôle global de la confidentialité (« Global Privacy Control » en anglais, ou GPC), est une préférence en matière de confidentialité que vous pouvez configurer dans votre navigateur Web pour informer les sites Web que vous ne souhaitez pas que vos Données à caractère personnel soient partagées ou vendues à des tiers indépendants sans votre consentement. GitLab reconnaît le GPC dans les juridictions où sa présence est requise par la loi en vigueur.
    - header: Programmes d'autorégulation
      id: self-regulatory-programs
      text: |
        Les prestataires de services peuvent participer à des programmes d’autorégulation qui offrent des moyens de désactiver les analyses et la publicité basée sur les centres d’intérêt, auxquels vous pouvez accéder ici :

        - États-Unis : NAI (<http://optout.networkadvertising.org>) et DAA (<http://optout.aboutads.info/>)
        - Canada : Digital Advertising Alliance of Canada (<https://youradchoices.ca/>)
        - Europe : Alliance européenne de la publicité numérique (<http://www.youronlinechoices.com>)
    - header: Balises Web
      id: web-beacons
      text: |
        La plupart des clients de messagerie ont des paramètres qui vous permettent d'empêcher le téléchargement automatique d'images, ce qui désactivera les balises Web dans les courriels que vous lisez.
    - header: Identifiants de publicité mobile
      id: mobile-advertising-ids
      text: |
        Sur les appareils mobiles, les identifiants publicitaires fournis par la plateforme peuvent être collectés et utilisés de la même manière que les identifiants de cookies. Vous pouvez utiliser les contrôles sur les systèmes d'exploitation iOS et Android qui vous permettent de limiter le suivi et/ou de remettre les identifiants publicitaires à zéro.
    - header: Quels types de cookies utilisons-nous?
      id: what-types-of-cookies-do-we-use
      text: |
        GitLab utilise quatre catégories de cookies : les cookies strictement nécessaires, les cookies fonctionnels, les cookies de performance et d'analyse, ainsi que les cookies de ciblage et de publicité. Vous pouvez en savoir plus à propos de chacun de ces types de cookies et voir les cookies de chaque catégorie qui se trouvent dans les Services en cliquant sur le lien **Préférences en matière de cookies** (intitulé **Ne pas vendre ou partager mes informations personnelles** pour les résidents de Californie) situé en bas de page de chaque site Web sous « Société ».
    